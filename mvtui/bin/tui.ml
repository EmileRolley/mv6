open LTerm_widget
open Lwt
open String
open Mvlib
open Mv
open Assembleur
open Myrthelib
open Compil

(* String representation of a state. *)
type string_state =
  { sacc : string
  ; (* accumulateur *)
    scode : string array
  ; (* programme    *)
    spc : string
  ; (* indice prochaine instruction *)
    sstack : string
  ; (* pile *)
    ssp : string (* indice du sommet de la pile *)
  }

let string_format_with c n s =
  (List.init (max 0 (n - length s)) (fun _ -> c) |> concat "") ^ s
;;

let fmt_bin = string_format_with "0" 8
let fmt_instr_nb = string_format_with " " 2

(* Returns the corresponding [string_state] from a given [state]. *)
let string_of_state state =
  { sacc = (if state.acc = Mv.undefined_val () then ".." else string_of_int state.acc)
  ; scode =
      Array.mapi
        (fun i inst ->
          "  "
          ^ fmt_instr_nb (string_of_int i)
          ^ " : "
          ^ string_of_instr inst
          ^ if i = state.pc - 1 then " *" else "")
        state.code
  ; spc = string_of_int state.pc
  ; sstack =
      state.stack
      |> Array.mapi (fun i e ->
             if e = Mv.undefined_val ()
             then ".."
             else (if i = state.sp then " * " else " ") ^ string_of_int e ^ " ")
      |> Array.fold_left (fun s e -> s ^ "" ^ e) ""
  ; ssp = string_of_int state.sp
  }
;;

(* Stores frames and labels. *)
let acc_i = 0
let code_i = 1
let stack_i = 2
let bytecode_i = 3
let myrthe_i = 4
let frames = Array.init 5 (fun _ -> new frame)

let labels =
  [| new label "      "; new label ""; new label ""; new label ""; new label "" |]
;;

(* Stores states. *)
let states_ref = ref [ Mv.init [||] [] ]
let myrthe_ref = ref ""
let states_i = ref 0
let get_curr_state () = List.nth !states_ref !states_i
let program_has_finished state = state.pc >= Array.length state.code

(* Inits the tui state. *)
let init_state_ref mvcode myrthexp with_myrthe =
  if with_myrthe
  then (
    states_ref := [ Mv.init (Array.of_list (compil myrthexp)) [] ];
    myrthe_ref := "\n" ^ MyrtheAST.string_of_exp 1 myrthexp)
  else states_ref := [ Mv.init mvcode [] ]
;;

let update_frames_with ?(expand = true) (label : string) (index : int) (box : box) : unit =
  frames.(index)#set_label label;
  frames.(index)#set labels.(index);
  box#add frames.(index) ~expand
;;

(* Inits the top vbox. *)
let init_state_vbox (state_vbox : vbox) : unit =
  (* Accumulator frame. *)
  update_frames_with "Acc" acc_i state_vbox ~expand:false;
  (* Stack frame. *)
  (* labels.(stack_i)#set_alignment H_align_right; *)
  update_frames_with "Stack" stack_i state_vbox;
  ()
;;

(* Inits the bottom vbox. *)
let init_code_hbox (code_hbox : hbox) (with_myrthe : bool) : unit =
  labels.(code_i)#set_alignment H_align_left;
  update_frames_with "Code" code_i code_hbox;
  labels.(bytecode_i)#set_alignment H_align_left;
  update_frames_with "Bytecode" bytecode_i code_hbox;
  if with_myrthe
  then (
    labels.(myrthe_i)#set_alignment H_align_left;
    update_frames_with "Myrthe-AST" myrthe_i code_hbox;
    labels.(myrthe_i)#set_text !myrthe_ref)
;;

(* Generates a string representation of the bytecode instructions (hexadecimal). *)
let generate_bytcode () =
  let bin_of_int d =
    if d < 0
    then invalid_arg "bin_of_int"
    else if d = 0
    then "0"
    else (
      let rec aux acc d =
        if d = 0 then acc else aux (string_of_int (d land 1) :: acc) (d lsr 1)
      in
      String.concat "" (aux [] d))
  in
  let state = get_curr_state () in
  let bytecode_to_string bytecode =
    List.init (length bytecode) (fun i ->
        let is_start = i mod 3 = 0 in
        let is_end = i mod 3 = 2 in
        let nb_instr = i / 3 in
        "  "
        ^ (if is_start then fmt_instr_nb (string_of_int nb_instr) ^ " : " else "")
        ^ fmt_bin (bin_of_int (Char.code bytecode.[i]))
        ^ if is_end then (if nb_instr = state.pc - 1 then " *" else "") ^ "\n" else "")
    |> concat ""
  in
  "\n" ^ (state.code |> assemble |> bytecode_to_string)
;;

(* Updates the content of labels according to the new [state_of_string]. *)
let update_labels (with_myrthe : bool) : unit =
  let state_of_string = string_of_state (get_curr_state ()) in
  state_of_string.sacc |> labels.(acc_i)#set_text;
  state_of_string.sstack |> labels.(stack_i)#set_text;
  state_of_string.scode
  |> Array.fold_left (fun acc s -> acc ^ "\n" ^ s) ""
  |> labels.(code_i)#set_text;
  generate_bytcode () |> labels.(bytecode_i)#set_text;
  if with_myrthe then ()
;;

(* Inits the main hbox with the [state_vbox] and the [code_vbox]. *)
let init_main_hbox
    (main_hbox : hbox)
    (state_vbox : vbox)
    (code_vbox : vbox)
    (with_myrthe : bool)
    wakener
    : unit
  =
  (* Adds the hboxes to the box. *)
  main_hbox#add state_vbox ~expand:false;
  main_hbox#add code_vbox;
  (* Defines the [on_event] function. *)
  main_hbox#on_event (function
      | LTerm_event.Key { LTerm_key.code = LTerm_key.Escape; _ } ->
        wakeup wakener ();
        true
      | LTerm_event.Key { LTerm_key.code = LTerm_key.Right; _ }
      | LTerm_event.Key { LTerm_key.code = LTerm_key.Down; _ } ->
        let curr_state = get_curr_state () in
        if not (program_has_finished curr_state)
        then (
          states_i := !states_i + 1;
          if !states_i >= List.length !states_ref
             (* Adds the new executed state at the end of [states_ref]. *)
          then states_ref := !states_ref @ [ Mv.exec_small curr_state ];
          update_labels with_myrthe);
        true
      | LTerm_event.Key { LTerm_key.code = LTerm_key.Left; _ }
      | LTerm_event.Key { LTerm_key.code = LTerm_key.Up; _ } ->
        if !states_i > 0
        then (
          states_i := !states_i - 1;
          update_labels with_myrthe);
        true
      | _ -> false)
;;
