open LTerm_widget
open Lwt
open Tui
open Mvlib
open Mv
open Myrthelib
open MyrtheAST

(* Is the array called by [Mv.init]. *)
let mvcode = [| Consti 6; Push; Consti 13; Push; Consti (-10) |]

(* Is the Myrthe expression to compile before calling [Mv.init]. *)
let myrthexp =
  If
    ( Binop (Eq, Const (Int 5), Binop (Plus, Const (Int 3), Const (Int 2)))
    , Const (Bool true)
    , Const (Bool false) )
;;

(* Main. *)
let main with_myrthe =
  let waiter, wakener = wait () in
  let state_vbox = new hbox in
  let code_hbox = new hbox in
  let main_hbox = new vbox in
  init_state_ref mvcode myrthexp with_myrthe;
  init_state_vbox state_vbox;
  init_code_hbox code_hbox with_myrthe;
  init_main_hbox main_hbox state_vbox code_hbox with_myrthe wakener;
  Lazy.force LTerm.stdout >>= fun term -> run term main_hbox waiter
;;

(* Entry point. *)
let () =
  try
    match Sys.argv.(1) with
    | "mv" -> Lwt_main.run (main false)
    | "myrthe" -> Lwt_main.run (main true)
    | arg -> Printf.printf "Unknow cmd '%s'. Try to use 'mv' or 'myrthe'" arg
  with
  | _ -> print_endline "Try to use 'mv' or 'myrthe'"
;;
