type instr =
  | Acc of int
  | Addi
  | Assign of int
  | Bgeint of int * int
  | Branchif of int
  | Branch of int
  | Consti of int
  | Eqi
  | Multi
  | Ori
  | Pop
  | Popi of int
  | Push

type state =
  { acc : int
  ; (* accumulateur *)
    code : instr array
  ; (* programme    *)
    pc : int
  ; (* indice prochaine instruction *)
    stack : int array
  ; (* pile *)
    sp : int (* indice du sommet de la pile *)
  }

let string_of_instr = function
  | Acc n -> "Acc " ^ string_of_int n
  | Addi -> "Addi"
  | Assign n -> "Assign " ^ string_of_int n
  | Bgeint (m, n) -> "Branchif " ^ string_of_int m ^ " " ^ string_of_int n
  | Branch n -> "Branch " ^ string_of_int n
  | Branchif n -> "Branchif " ^ string_of_int n
  | Consti n -> "Consti " ^ string_of_int n
  | Eqi -> "Eqi"
  | Ori -> "Ori"
  | Pop -> "Pop"
  | Popi n -> "Popi " ^ string_of_int n
  | Push -> "Push"
  | Multi -> "Multi"
;;

let undefined_val _ = 42

(* creation etat initial d'un programme sur des entrees *)
let init (p : instr array) (args : int list) : state =
  { acc =
      (match args with
      | [] -> undefined_val () (* valeur quelconque *)
      | h :: _ -> h (* valeur en entree  *))
  ; code = p
  ; pc = 0
  ; stack =
      (let args = List.rev args (* respecter ordre arguments dans pile *) in
       Array.init
         (List.length args + 50)
         (fun i ->
           if i < List.length args - 1
           then List.nth args i
           else undefined_val () (* valeur quelconque *)))
  ; sp = max (-1) (List.length args - 2)
  }
;;

(* execution a petits pas *)
let exec_small (m : state) : state =
  let stack = Array.copy m.stack in
  let pc = m.pc + 1 in
  match m.code.(m.pc) with
  | Push ->
    let sp = m.sp + 1 in
    stack.(sp) <- m.acc;
    { acc = m.acc; code = m.code; pc; stack; sp }
  | Pop -> { acc = m.stack.(m.sp); code = m.code; pc; stack; sp = m.sp - 1 }
  | Consti n -> { acc = n; code = m.code; pc; stack; sp = m.sp }
  | Addi -> { acc = m.stack.(m.sp) + m.acc; code = m.code; pc; stack; sp = m.sp - 1 }
  | Ori ->
    { acc = (if m.stack.(m.sp) = 0 && m.acc = 0 then 0 else 1)
    ; code = m.code
    ; pc
    ; stack
    ; sp = m.sp - 1
    }
  | Eqi ->
    { acc = (if m.stack.(m.sp) = m.acc then 1 else 0)
    ; code = m.code
    ; pc
    ; stack
    ; sp = m.sp - 1
    }
  | Acc n -> { acc = stack.(m.sp - n); code = m.code; pc; stack; sp = m.sp }
  | Assign n ->
    stack.(n - 1) <- m.acc;
    { acc = 0; code = m.code; pc; stack; sp = m.sp }
  | Popi n -> { acc = m.acc; code = m.code; pc; stack; sp = m.sp - n }
  | Branch n -> { acc = m.acc; code = m.code; pc = m.pc + n; stack; sp = m.sp }
  | Branchif n ->
    { acc = m.acc
    ; code = m.code
    ; pc = (if m.acc = 1 then m.pc + n else pc)
    ; stack
    ; sp = m.sp
    }
  | Bgeint (m', n) ->
    { acc = m.acc
    ; code = m.code
    ; pc = (if m.acc >= m' then pc + n else pc)
    ; stack
    ; sp = m.sp
    }
  | Multi -> { acc = m.stack.(m.sp) * m.acc; code = m.code; pc; stack; sp = m.sp - 1 }
;;

(* { acc = stack.(0); code = m.code; pc; stack; sp = m.sp } *)

(* execution a grand pas *)
let rec exec_big (m : state) : state =
  if m.pc < Array.length m.code then exec_big (exec_small m) else m
;;

(* execution a grand pas, version equivalente *)
let exec_big_loop (m : state) : state =
  while m.pc < Array.length m.code do
    let _ = exec_small m in
    ()
  done;
  m
;;
