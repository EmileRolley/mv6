open Mv
open String

let instr_to_n = function
  | Acc _ -> 6
  | Addi -> 1
  | Assign _ -> 10
  | Bgeint _ -> 11
  | Branch _ -> 8
  | Branchif _ -> 9
  | Consti _ -> 4
  | Eqi -> 2
  | Multi -> 12
  | Ori -> 3
  | Pop -> 5
  | Popi _ -> 7
  | Push -> 0
;;

(*assembleur pour programmes mv*)
let assemble (p : instr array) : string =
  let create_signed_list i n =
    [ Char.chr i; Char.chr (if n >= 0 then 0 else 1); Char.chr (abs n) ]
  in
  let create_solo_list n = create_signed_list n 0 in
  let concatmap_to_string_list = List.concat_map (fun c -> [ make 1 c ]) in
  let encode i =
    match i with
    | Push | Addi | Eqi | Ori | Pop | Multi -> i |> instr_to_n |> create_solo_list
    | Acc n -> create_signed_list (instr_to_n i) n
    | Assign n -> create_signed_list (instr_to_n i) n
    | Consti n -> create_signed_list (instr_to_n i) n
    | Popi n -> create_signed_list (instr_to_n i) n
    | Branch n -> create_signed_list (instr_to_n i) n
    | Branchif n -> create_signed_list (instr_to_n i) n
    | Bgeint _ -> failwith "unsported instruction : Bgeint"
  in
  Array.fold_right
    (fun i s ->
      (encode i |> concatmap_to_string_list |> List.fold_left (fun s' c -> s' ^ c) "") ^ s)
    p
    ""
;;

(* desassembleur pour programmes mv *)

let disassemble (s : string) : instr array =
  let open Char in
  let get_n c2 c3 = if 1 = code c2 then -1 * code c3 else code c3 in
  let decode c1 c2 c3 =
    match code c1 with
    | 0 -> Push
    | 1 -> Addi
    | 2 -> Eqi
    | 3 -> Ori
    | 4 -> Consti (get_n c2 c3)
    | 5 -> Pop
    | 6 -> Acc (get_n c2 c3)
    | 7 -> Popi (get_n c2 c3)
    | 8 -> Branch (get_n c2 c3)
    | 9 -> Branchif (get_n c2 c3)
    | 10 -> Assign (get_n c2 c3)
    | 11 -> Multi
    | _ -> failwith "invalid byte-code"
  in
  Array.init (length s) (fun i ->
      if i mod 3 = 0 && i < length s - 3
      then Some (decode s.[i] s.[i + 1] s.[i + 2])
      else None)
  |> Array.to_list
  |> List.filter_map (fun o ->
         match o with
         | Some i -> Some i
         | None -> None)
  |> Array.of_list
;;

(*examples*)
let p1 = [| Consti 2; Push; Consti 3; Addi; Pop |]
let p2 = [| Consti 2; Push; Addi; Pop; Pop |]
let p3 = [| Consti 2; Push; Consti 3; Push; Addi; Addi |]
