(*precompiler myrtheAST.ml et mv.ml
* avec ocamlc -c myrtheAST.ml mv.ml
* charger le module dans le top-level avec
* #load "myrtheAST.cmo" et #load "myrthe.cmo" et #load "mv.cmo"
**)
open MyrtheAST
open Mvlib
open Mv

let repr : value -> int = function
  | Bool true -> 1
  | Bool false -> 0
  | Int i -> i
;;

let get_acc (s : state) : int = s.acc

let rec compil = function
  | Const v -> [ Consti (repr v) ]
  | Unop (Succ, e) -> compil e @ [ Push; Consti 1; Addi ]
  | Binop (Plus, e1, e2) -> compil e1 @ [ Push ] @ compil e2 @ [ Addi ]
  | Binop (Eq, e1, e2) -> compil e1 @ [ Push ] @ compil e2 @ [ Eqi ]
  | If (e1, e2, e3) ->
    let l1 = compil e1 in
    let l2 = compil e2 in
    let l3 = compil e3 in
    l1 @ [ Branchif (List.length l3 + 2) ] @ l3 @ [ Branch (List.length l2 + 1) ] @ l2
  | _ -> failwith "Students, this is your job !"
;;
