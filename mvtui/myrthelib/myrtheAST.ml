type value =
  | Int of int
  | Bool of bool

and var_id = string

and unop =
  | Not
  | Succ

and binop =
  | Plus
  | Times
  | Et
  | Or
  | Eq
  | Leq
  | Minus

and expression =
  | Const of value
  | Unop of unop * expression
  | Binop of binop * expression * expression
  | If of expression * expression * expression
  | Var of var_id
  | Let of var_id * expression * expression

let string_of_value = function
  | Int i -> "Int " ^ string_of_int i
  | Bool b -> "Bool " ^ string_of_bool b
;;

let string_of_unop = function
  | Not -> "Not"
  | Succ -> "Succ"
;;

let string_of_binop = function
  | Plus -> "Plus"
  | Times -> "Times"
  | Et -> "Et"
  | Or -> "Or"
  | Eq -> "Eq"
  | Leq -> "Leq"
  | Minus -> "Minus"
;;

let rec string_of_exp indent exp =
  let i = indent * 2 in
  let newi = indent + 1 in
  String.make i ' '
  ^
  match exp with
  | Const v -> "Const (" ^ string_of_value v ^ ")"
  | Unop (op, e) -> "Unop (" ^ string_of_unop op ^ ", " ^ string_of_exp indent e ^ ")"
  | Binop (op, e1, e2) ->
    "Binop (\n  "
    ^ String.make i ' '
    ^ string_of_binop op
    ^ ",\n"
    ^ string_of_exp newi e1
    ^ ",\n"
    ^ string_of_exp newi e2
    ^ "\n"
    ^ String.make i ' '
    ^ ")"
  | If (e1, e2, e3) ->
    "If (\n"
    ^ string_of_exp newi e1
    ^ ",\n"
    ^ string_of_exp newi e2
    ^ ",\n"
    ^ string_of_exp newi e3
    ^ "\n"
    ^ String.make i ' '
    ^ ")"
  | Var v -> "Var (" ^ v ^ ")"
  | Let (v, e1, e2) ->
    "Let (" ^ v ^ ", " ^ string_of_exp indent e1 ^ ", " ^ string_of_exp indent e2 ^ ")"
;;

(*
let exp1 = failwith "Students, this is your job !"
let exp2 = failwith "Students, this is your job !"
let exp3 = failwith "Students, this is your job !"
let exp4 = failwith "Students, this is your job !"
*)
