# Tools for the course *Machines virtuelles*


<!-- vim-markdown-toc GFM -->

* [`mvtui` 🐫](#mvtui-)
	* [Dependency](#dependency)
	* [How to..](#how-to)
		* [build](#build)
		* [execute](#execute)
		* [use](#use)
	* [TODOs](#todos)

<!-- vim-markdown-toc -->

Link to the course : https://www.irif.fr/~haberm//cours/mv/

## `mvtui` 🐫

Simple terminal interface for debugging the virtual machine.

<img src="/uploads/4213a335bbaf14bc8f67874405dccb5b/Screenshot_from_2021-02-11_10-10-23.png"/>

### Dependency

[Lamda-Term](https://github.com/ocaml-community/lambda-term) is used to build the TUI.

It could be installed with [opam](https://opam.ocaml.org/) :

```opam
opam install lambda-term
```

or built from sources.

### How to..

#### build

```shell
cd mvtui && dune build
```

#### execute

In order to use the hard coded `Mv.instr array` :

```shell
dune exec mvtui -- mv
```

For using the `Myrthe` expression :

```shell
dune exec mvtui -- myrthe
```

> Both of the hard coded values could be modified in the file *./mvtui/bin/main.ml*
>
> ```ocaml
> (* Is the array called by [Mv.init]. *)
> let mvcode = [| Consti 6; Push; Consti 13; Push; Consti (-10) |]
>
> (* Is the Myrthe expression to compile before calling [Mv.init]. *)
> let myrthexp =
>   If
>     ( Binop (Eq, Const (Int 5), Binop (Plus, Const (Int 3), Const (Int 2)))
>     , Const (Bool true)
>     , Const (Bool false) )
> ;;
>
> ```

#### use

Once it's running, just press ⇨ or ⇩ for moving one step forward and
⇦ or ⇧ in order to move one step backward.

To exit press `esc`.

### TODOs

* [ ] File and/or argument parsing in order to avoid the hard coded init state.
* [ ] Show more current state attributes ?
* [ ] Print the log when exiting ?
* [ ] More fancy style (color/font style) ?
